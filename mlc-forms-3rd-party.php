<?php
/*
Plugin Name: Forms: 3rd-Party - MyLeadConverter Integration
Plugin URI: http://www.myleadconverter.com
Description: Create new leads in MyLeadConverter via Forms 3rd Party Plugin.
Author: MyLeadConverter
Author URI: http://www.myleadconverter.com
Version: 1.0.0
*/

if (!defined('MLC_FORMS_3RD_PARTY_BASENAME')) {
    define('MLC_FORMS_3RD_PARTY_BASENAME', plugin_basename( __FILE__ ));
}

new MLC_Forms_3rd_Party();



final class MLC_Forms_3rd_Party {

    const ACCOUNT_ID_SETTING = 'mlc_account_id';
    const AUTH_TOKEN_SETTING = 'mlc_auth_token';
    const CALL_TRACKING_ENABLE_SETTING = 'mlc_call_tracking_enable';
    const CALL_TRACKING_DEBUG_SETTING = 'mlc_call_tracking_debug';
    const AFTER_REPLACEMENT_JS_SETTING = 'mlc_after_replacement';

    private $cf7Forms = array(
        1000 => '',
    );

    public function __construct() {
        add_filter('Forms3rdPartyIntegration_service_filter_args', array($this, 'alter3rdPartyArgs'), 10, 3);
        add_filter('plugin_action_links_' . MLC_FORMS_3RD_PARTY_BASENAME, array($this, 'getActionLinks'));

        add_action('wp_footer', array($this, 'appendScript')); // append mlc.js to footer of each page
        add_action('admin_menu', array($this, 'adminMenu'));
    }

    public function alter3rdPartyArgs( $args, $svc, $cf7 ) {

        $cf7Id = $cf7->id;

        if (!array_key_exists($cf7Id, $this->cf7Forms)) {

            return $args;

        } else {

            $existingHeaders = !array_key_exists('headers', $args) ? array() : $args['headers'];
            $existingBody = !array_key_exists('body', $args) ? array() : $args['body']; // mapped via Forms 3rd Party UI
            $posted = WPCF7_Submission::get_instance()->get_posted_data(); // all posted data from form

            $mlcCookie = array();
            parse_str(isset($_COOKIE['__mlc']) ? $_COOKIE['__mlc'] : '', $mlcCookie);
            $existingBody = array_merge($existingBody, $mlcCookie);

            return array_merge($args, array(
                'headers' => array_merge($existingHeaders, array(
                    'Authorization' => 'Basic ' . base64_encode( $this->getAccountId() . ':' . $this->getAuthToken() ),
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'X-API-Version' => '011712',
                    'Accept' => 'application/json'
                )),
                'body' => array_merge($posted, $existingBody, array(
                    'mlc__fieldset' => $this->cf7Forms[$cf7Id]
                ))
            ));

        }
    }

    /**
     * @param array $links The default action links.
     * @return array The action links.
     */
    public function getActionLinks( $links ) {
        $settingsPage = esc_attr(admin_url('options-general.php?page=myleadconverter-settings'));
        $settingsLink = "<a href=\"$settingsPage\">"
            . esc_html(__( 'Settings', 'wpcf7' ))
            . '</a>';

        array_unshift($links, $settingsLink);
        return $links;
    }

    public function adminMenu() {
        add_options_page('Edit MyLeadConverter Integration Settings', 'MyLeadConverter'
            , 'manage_options', 'myleadconverter-settings', array($this, 'settingsPage'), plugins_url('/images/icon.png', __FILE__) );

        add_action( 'admin_init', array($this, 'registerSettings'));
    }

    public function registerSettings() {
        register_setting( 'mlc-settings-group', self::ACCOUNT_ID_SETTING );
        register_setting( 'mlc-settings-group', self::AUTH_TOKEN_SETTING );
        register_setting( 'mlc-settings-group', self::CALL_TRACKING_ENABLE_SETTING );
        register_setting( 'mlc-settings-group', self::CALL_TRACKING_DEBUG_SETTING );
        register_setting( 'mlc-settings-group', self::AFTER_REPLACEMENT_JS_SETTING );
    }

    public function settingsPage() {
        ?>
        <div class="wrap">
            <h2>MyLeadConverter Settings</h2>
            <form method="post" action="options.php">
                <?php settings_fields( 'mlc-settings-group' ); ?>
                <?php do_settings_sections( 'mlc-settings-group' ); ?>

                <h3>API Credentials</h3>
                <table class="form-table">
                    <tr valign="top">
                    <th scope="row"><label for="<?php echo self::ACCOUNT_ID_SETTING; ?>">Account ID:</label></th>
                    <td><input type="text"
                               name="<?php echo self::ACCOUNT_ID_SETTING; ?>"
                               id="<?php echo self::ACCOUNT_ID_SETTING; ?>"
                               value="<?php echo esc_attr( $this->getAccountId() ); ?>" /></td>
                    </tr>

                    <tr valign="top">
                    <th scope="row"><label for="<?php echo self::AUTH_TOKEN_SETTING; ?>">Auth Token:</label></th>
                    <td><input type="text"
                               name="<?php echo self::AUTH_TOKEN_SETTING; ?>"
                               id="<?php echo self::AUTH_TOKEN_SETTING; ?>"
                               value="<?php echo esc_attr( $this->getAuthToken() ); ?>" /></td>
                    </tr>
                </table>

                <br>

                <h3>Call Tracking</h3>
                <table class="form-table">
                    <tr valign="top">
                    <th scope="row"><label for="<?php echo self::CALL_TRACKING_ENABLE_SETTING; ?>">Enable Call Tracking:</label></th>
                    <td><input type="checkbox"
                               name="<?php echo self::CALL_TRACKING_ENABLE_SETTING; ?>"
                               id="<?php echo self::CALL_TRACKING_ENABLE_SETTING; ?>"
                               value="1" <?php checked($this->getCallTrackingEnable(), 1); ?> /></td>
                    </tr>

                    <tr valign="top">
                    <th scope="row"><label for="<?php echo self::CALL_TRACKING_DEBUG_SETTING; ?>">Debug:</label></th>
                    <td><input type="checkbox"
                               name="<?php echo self::CALL_TRACKING_DEBUG_SETTING; ?>"
                               id="<?php echo self::CALL_TRACKING_DEBUG_SETTING; ?>"
                               value="1" <?php checked($this->getCallTrackingDebug(), 1); ?> /></td>
                    </tr>

                    <tr valign="top">
                    <th scope="row"><label for="<?php echo self::AFTER_REPLACEMENT_JS_SETTING; ?>">After Phone Replacement:</label></th>
                    <td>
                        <textarea name="<?php echo self::AFTER_REPLACEMENT_JS_SETTING; ?>"
                                  id="<?php echo self::AFTER_REPLACEMENT_JS_SETTING; ?>"
                                  rows="4" style="width:100%;font-family:monospace"><?php echo stripslashes($this->getAfterReplacementJs()); ?></textarea>
                        <small style="font-family:monospace">ex. function(data) { console.log(data) }</small>
                    </tr>
                </table>

                <?php submit_button(); ?>
            </form>
        </div>
        <?php
    }

    /**
     * Append the MyLeadConverter javascript code to the footer of each page.
     */
    public function appendScript() {
        $opts = array(
            'phoneReplacementActive' => ($this->getCallTrackingEnable() == 1 ? 'true' : 'false'),
            'searchWithin' => array('a[href^=tel]'),
            'excludeNumbers' => array(),
            'onlyNumbers' => array(),
            'debug' => ($this->getCallTrackingDebug() == 1 ? 'true' : 'false'),
            'tollFreeCanBeLocal' => 'true',
            'localCanBeTollFree' => 'true'
        );
        $searchWithin = $this->mk_js_array($opts['searchWithin']);
        $excludeNumbers = $this->mk_js_array($opts['excludeNumbers']);
        $onlyNumbers = $this->mk_js_array($opts['onlyNumbers']);
        $afterReplacement = stripslashes($this->getAfterReplacementJs());

        echo "\n<!-- MyLeadConverter Integration -->\n"
            . '<script type="text/javascript">(function(d, w, t) {'
            . "\n". 'w.myleadconverter = {phone : (function(){ return {'
            . 'active:' . $opts['phoneReplacementActive']
            . ',within:[' . $searchWithin . ']'
            . ',exclude:[' . $excludeNumbers . ']'
            . ',only:[' . $onlyNumbers . ']'
            . ',debug:' . $opts['debug']
            . ',tollFreeCanBeLocal:' . $opts['tollFreeCanBeLocal']
            . ',localCanBeTollFree:' . $opts['localCanBeTollFree']
            . ',afterReplace: ' . ($afterReplacement == '' ? 'function(){}' : $afterReplacement)
            . '} }())};' . "\n"
            . 'var m = d.createElement(t), s = d.getElementsByTagName(t)[0];'
            . 'm.src = "//www.myleadconverter.com/mlc.js?d="+w.location.href;'
            . 's.parentNode.insertBefore(m,s);'
            . '}(document, window, "script"));</script>'
            . "\n\n";
    }

    public static function getPluginUrl( $path = '' ) {
        return plugins_url($path, MLC_FORMS_3RD_PARTY_BASENAME);
    }

    private function array_fuzzy_search( $arr, $in ) {
        $tmpkeys = array();
        $keys = array_keys($arr);
        foreach ($keys as $k) {
            if (stripos($k, $in) !== FALSE) $tmpkeys[] = $k;
        }
        return $tmpkeys;
    }

    private function mk_js_array( $arr ) {
      return join(',', array_map(function($v){ return "'".$v."'"; }, $arr));
    }

    private function getAccountId() {
        return get_option(self::ACCOUNT_ID_SETTING);
    }

    private function getAuthToken() {
        return get_option(self::AUTH_TOKEN_SETTING);
    }

    private function getCallTrackingEnable() {
        return get_option(self::CALL_TRACKING_ENABLE_SETTING);
    }

    private function getCallTrackingDebug() {
        return get_option(self::CALL_TRACKING_DEBUG_SETTING);
    }

    private function getAfterReplacementJs() {
        return get_option(self::AFTER_REPLACEMENT_JS_SETTING);
    }

}