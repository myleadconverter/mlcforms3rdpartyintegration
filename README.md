# MyLeadConverter WordPress Plugin

The WordPress plugin sends new leads to MyLeadConverter via "Forms: 3rd-party Integration" plugin.

## Description
**This plugin requires [Contact Form 7](https://wordpress.org/plugins/contact-form-7/), version 4.2+**

**This plugin requires [Forms: 3rd-Party Integration](https://wordpress.org/plugins/forms-3rdparty-integration/), version 1.6.6+**

- - -

### Requirements

#### WordPress
This plugin was built and tested on WordPress version 4.3.1.

#### Contact Form 7
Contact Form 7 allows you to configure your forms. View the Contact Form 7 [docs](http://contactform7.com/docs/) for more information.

#### Forms: 3rd-Party Integration
Forms 3rd-Party captures data from Contact Form 7 and marshals that data to the MyLeadConverter plugin.

#### MyLeadConverter Account
A MyLeadConverter account will allow you to track and manage sales leads from both online and offline lead sources. MyLeadConverter can track and record phone calls and let you know where those
phone calls are coming from.

## Installation

1. Download the [mlc-forms-3rd-party.zip](https://bitbucket.org/amullins/mlcforms3rdpartyintegration/downloads/mlc-forms-3rd-party.zip) file
1. Activate the plugin through `Plugins > Add New` "Upload Plugin" button in WordPress admin
1. Obtain Account ID, API Token and your fieldset IDs from MyLeadConverter
1. Configure the MyLeadConverter section on the Contact Form 7 form settings page
